var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');


/* GET home page. */
router.get('/',ensureAuthenticated, function(_req, res, next) {
  res.render('index');
});

 router.get('/get-data', function(req, res){
   var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// Connection URL
  var url = 'mongodb://localhost:27017/loginapp';
 // Use connect method to connect to the Server
 MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");

  db.close();
 });

});

router.get('/insert', function(req, res, next){
  var item = {
    title: req.body.title,
    content: req.body.content,
    author: req.body.author
  }
})

function ensureAuthenticated(req, res, next){
  if(req.isAuthenticated()){
    return next();
  }else{
    //req.flash('error_msg', 'You are not logged in');
    res.redirect('/users/login');
  }
}
module.exports = router;
