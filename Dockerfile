# Create a node 10 image using alpine version linux
FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

# expose to 8080 port
EXPOSE 8080
CMD [ "node", "app.js" ]
